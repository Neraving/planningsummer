﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Engine
{
    public class MarkerStruct
    {
        // TODO protected set ???
        /// <summary>
        /// Маркеры, связанные дизъюнкцией (ИЛИ)
        /// Внутренний список - Маркеры, связанные конъюнкцией (И)
        /// </summary>
        public List<List<Marker>> Disjunction { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public MarkerStruct()
        {
            Disjunction = new List<List<Marker>>();
        }

        /// <summary>
        /// Добавление маркера к условию И
        /// </summary>
        /// <param name="newMarker">Добавляемый маркер</param>
        /// <param name="marker">Маркер, по которому ищется пополняемый список</param>
        public void AddConjunction(Marker newMarker, Marker marker)
        {
            List<Marker> list = FindMarker(marker);
            if (list.Count > 0)
            {
                DeleteList(list); 
                list.Add(newMarker);
                Disjunction.Add(list);
            }
        }

        /// <summary>
        /// Добавление списка маркеров к условию И
        /// </summary>
        /// <param name="newMarker">Список добавляемых маркеров</param>
        /// <param name="marker">Маркер, по котормуй ищется пополняемый список</param>
        public void AddConjunction(List<Marker> newMarker, Marker marker)
        {
            List<Marker> list = FindMarker(marker);
            if (list.Count > 0)
            {
                DeleteList(list);
                list.AddRange(newMarker);
                Disjunction.Add(list);
            }
        }

        /// <summary>
        /// Поиск списка, в котором содержится маркер
        /// </summary>
        /// <param name="marker">Маркер</param>
        /// <returns>Список, содержащий данный маркер</returns>
        public List<Marker> FindMarker(Marker marker)
        {
            // перебор поэлементно внешнего списка (дизъюнкция)
            foreach (List<Marker> list in Disjunction)
            {
                // перебор поэлементно внутренненго списка (конъюнкция)
                foreach (Marker t in list)
                    if (t == marker)
                        return list;
            }
            return new List<Marker>();
        }

        /// <summary>
        /// Удаление списка 
        /// </summary>
        /// <param name="list">Удаляемый список</param>
        public void DeleteList(List<Marker> list)
        {
            Disjunction.Remove(list);
        }

        // TODO написать подобный метод, но с маркерами, связанными дизъюнкцией. Возможно переименовать этот
        /// <summary>
        /// Добавление списка маркеров к условию ИЛИ.
        /// Маркеры в передаваемом списке связаны конъюнкцией
        /// </summary>
        /// <param name="markers">Добавляемый список маркеров</param>
        public void AddDisjunction(List<Marker> markers)
        {
            Disjunction.Add(markers);
        }

        /// <summary>
        /// Добавление одного маркера к условию ИЛИ
        /// </summary>
        /// <param name="marker">Добавляемый маркер</param>
        public void AddDisjunction(Marker marker)
        {
            var markers = new List<Marker> { marker };
            Disjunction.Add(markers);
        }

        /// <summary>
        /// Удаление всех связей с маркером из структуры связей
        /// </summary>
        /// <param name="marker">маркер</param>
        public void RemoveElement(Marker marker)
        {
            // перебор поэлементно внешнего списка (дизъюнкция)
            foreach (var list in this.Disjunction)
            {
                // удаление требуемого маркера из конъюнкции, если он есть в списке
                if ((list.Find(t=>t.Goal ==marker.Goal))!=null) list.RemoveAll(t => t.Goal == marker.Goal);  
            }

            // удаление из дизъюнкции пустых списков
            for (int i =0;i<Disjunction.Count;i++)
            {
                if (Disjunction[i].Count==0)
                {
                    Disjunction.RemoveAt(i);
                    i--;
                }
            }
        }
    }
}
