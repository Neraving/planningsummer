﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AbstractGeometry;
using AbstractModels;
using Engine.Planner;
using FPModels;
using FPModels.Messages;
using FPModels.Plan;
using Variables;

namespace Engine
{
    /// <summary>
    /// Маркер на карте, содержащий фигуру и её задачи
    /// </summary>
    public class Marker
    {
        /// <summary>
        /// Задача
        /// </summary>
        public string Goal { get; protected set; }

        //TODO подумать, что это и НАДО ЛИ
        public FPPlan MarkerPlan { get; protected set; }

        //TODO подумать, что это и надо ли
        public FPAction Action { get; protected set; }

        /// <summary>
        /// Требования к модели
        /// </summary>
        public List<AbleToAction> DemandModel { get; protected set; }

        /// <summary>
        /// Контекст
        /// </summary>
        public СonditionsСontext СonditionsСontext { get; protected set; }

        /// <summary>
        /// Время создания
        /// </summary>
        public DateTime Time { get; protected set; }

        /// <summary>
        /// Позиця фигуры
        /// </summary>
        public IFigure Position { get; set; }

        /// <summary>
        /// Структура задач, содержит задачи, связанные дизъюнкицией и конъюнкцией
        /// </summary>
        public MarkerStruct MarkerStructure { get; set; }

        #region конструкторы

        /// <summary>
        /// Конструтор маркера при известной его позиции
        /// </summary>
        /// <param name="position">Позиция маркера</param>
        public Marker(IFigure position)
        {
            Position = position;
            MarkerStructure = new MarkerStruct();
            DemandModel = new List<AbleToAction>();
            Time = DateTime.Now;
        }

        /// <summary>
        /// Конструктор маркера при известной его позиции и его задаче
        /// </summary>
        /// <param name="position">Позиция маркера</param>
        /// <param name="goal">Задача маркера</param>
        public Marker(IFigure position, string goal)
        {
            Position = position;
            Goal = goal;
            MarkerStructure = new MarkerStruct();
            DemandModel = new List<AbleToAction>();
            Time = DateTime.Now;
        }

        // возможно лишние конструторы
        ///// <summary>
        ///// Конструктор маркера при известной его позиции, задаче и подзадачам
        ///// </summary>
        ///// <param name="position">Позиция маркера</param>
        ///// <param name="goal">Задача маркера</param>
        ///// <param name="taskStruct">Связанные задачи</param>
        //public Marker(IFigure position, string goal, MarkerStruct markerStructure)
        //{
        //    Position = position;
        //    Goal = goal;
        //    MarkerStructure = markerStructure;
        //    Time = DateTime.Now;
        //}

        ///// <summary>
        ///// Конструктор маркера при известной его позиции, задаче и подзадачам
        ///// </summary>
        ///// <param name="position">Позиция маркера</param>
        ///// <param name="goal">Задача маркера</param>
        ///// <param name="taskStruct">Связанные задачи</param>
        //public Marker(IFigure position, string goal, MarkerStruct markerStruct, 
        //    List<AbleToAction> demandModel, СonditionsСontext context, FPAction action)
        //{
        //    Position = position;
        //    Goal = goal;
        //    MarkerStructure = markerStruct;
        //    this.DemandModel = demandModel;
        //    СonditionsСontext = context;
        //    Action = action;
        //    Time = DateTime.Now;
        //}
        #endregion


        /// <summary>
        /// Перенос маркера (смена позиции на карте)
        /// </summary>
        /// <param name="newPosition">Новая позиция</param>
        public void TransferMarker(IFigure newPosition)
        {
            Position = newPosition;
        }

        #region Context

        /// <summary>
        /// Заменить контекст
        /// </summary>
        /// <param name="context">Новый контекст</param>
        public void ChangeContext(СonditionsСontext context)
        {
            СonditionsСontext = context;
        }



        // TODO ниггеры, подавать object предикаты, птмчт у ConstrainedVariable protected конструктор
        /// <summary>
        /// Добавить переменную в контекст
        /// </summary>
        /// <param name="name">Название переменной</param>
        /// <param name="value">Переменная</param>
        public void AddContext(string name, ConstrainedVariable value)
        {
            СonditionsСontext[name] = value;
        }
        /// <summary>
        /// Добавить переменную в контекст
        /// </summary>
        /// <param name="position">Номер переменной</param>
        /// <param name="value">Переменная</param>
        public void AddContext(int position, ConstrainedVariable value)
        {
            СonditionsСontext[position] = value;
        }




        /// <summary>
        /// Удаление контекста из маркера
        /// </summary>
        public void DeleteContext()
        {
            СonditionsСontext = new СonditionsСontext();
        }

        /// <summary>
        /// Удаление переменной из контекста по имени
        /// </summary>
        /// <param name="deletingName">Название удаляемой переменной</param>
        public void DeleteContext(string deletingName)
        {
            СonditionsСontext[deletingName] = null;
        }

        /// <summary>
        /// Удаление переменной из контекста по номеры
        /// </summary>
        /// <param name="deletingNumber">Номер удаляемой переменной</param>
        public void DeleteContext(int deletingNumber)
        {
            СonditionsСontext[deletingNumber] = null;
        }

        #endregion

        #region DemandModel

        /// <summary>
        /// Добавить требование к модели
        /// </summary>
        /// <param name="demand">Требование к модели</param>
        public void AddDemandModel(AbleToAction demand)
        {
            if (!DemandModel.Contains(demand))
            {
                DemandModel.Add(demand);
            }
        }

        /// <summary>
        /// Добавить список требований к модели
        /// </summary>
        /// <param name="demands">Список требований к модели</param>
        public void AddDemandModel(List<AbleToAction> demands)
        {
            DemandModel = new List<AbleToAction>(demands); 
        }

        /// <summary>
        /// Удалить требование к модели
        /// </summary>
        /// <param name="deletedDemand">Удаляемое требование</param>
        public void DeleteDemandModel(AbleToAction deletedDemand)
        {
            DemandModel.Remove(deletedDemand);
        }

        /// <summary>
        /// Удалить все требования к модели
        /// </summary>
        public void DeleteDemandModel()
        {
            DemandModel = new List<AbleToAction>();
        }

        #endregion

        #region Action

        /// <summary>
        /// Создание действия
        /// </summary>
        public void CreateAction()
        {
            Action = new FPAction();
        }

        /// <summary>
        /// Прикрепление действия к задаче.
        /// В задаче может содержаться либо план, либо действие.
        /// </summary>
        /// <param name="action">Действие</param>
        public void AddAction(FPAction action)
        {
            if (MarkerPlan == null)
                Action = action;
        }

        /// <summary>
        /// Удаление действия из задачи
        /// </summary>
        public void DeleteAction()
        {
            Action = null;
        }

        #endregion

        #region Plan

        /// <summary>
        /// Создание плана 
        /// </summary>
        public void CreatePlan()
        {
            MarkerPlan = new FPPlan();
        }

        /// <summary>
        /// Добавление плана к маркеру.
        /// В маркере может содержаться либо план, либо действие.
        /// </summary>
        /// <param name="plan">Добавляемый план</param>
        public void AddPlan(FPPlan plan)
        {
            if (Action == null)
                MarkerPlan = plan;
        }

        /// <summary>
        /// Удаление плана из маркера 
        /// </summary>
        public void DeletePlan()
        {
            MarkerPlan = null;
        }

        /// <summary>
        /// Загрузка плана из хранилища
        /// </summary>
        /// <param name="message">Сообщение с планом</param>
        /// <returns></returns>
        public void LoadPlan(PlanMessage message)
        {
            MarkerPlan = message.Plan;
        }

        /// <summary>
        /// TODO Сохранение плана в хранилище
        /// </summary>
        /// <param name="plan">Сохраняемый план</param>
        public void SavePlan(FPPlan plan)
        {
            //foreach (var colleague in Colleagues)
            //{
            //    PlanMessage message=new PlanMessage(plan,fppocId,colleague);
            //    SendMessage(message);
            //}
        }

        #endregion
    }
}
