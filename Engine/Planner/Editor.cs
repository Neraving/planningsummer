﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AbstractGeometry;
using AbstractModels;
using FPGeometry;
using FPMap;
using FPModels;
using FPModels.Messages;
using FPModels.Plan;
using Variables;

namespace Engine.Planner
{
    public class Editor
    {
        private Dictionary<Marker, MarkerStruct> PlanOfMarker = new Dictionary<Marker, MarkerStruct>();
        public Dictionary<FPGoal, GoalStruct> Plan { get; set;} 
        
        /// <summary>
        /// Конструктор (исп. для модульного тестирования)
        /// </summary>
        /// <param name="p">сформированный список маркеров и связей между ними</param>
        public Editor (Dictionary<Marker, MarkerStruct> p)
        {
            PlanOfMarker = p;
            Plan = new Dictionary<FPGoal, GoalStruct>();
        }


        #region Marker
        /// <summary>
        /// Добавление маркера по позиции и цели
        /// </summary>
        /// <param name="position">Позиция на карте</param>
        /// /// <param name="goal">Задача</param>
        /// <returns>Маркер</returns>
        public void AddMarker(IFigure position, string goal)
        {
            PlanOfMarker.Add(new Marker(position, goal), new MarkerStruct());
        }

        /// <summary>
        /// Добавление маркера по позиции
        /// </summary>
        /// <param name="position">Позиция на карте</param>
        /// <returns>Маркер</returns>
        public void AddMarker(IFigure position)
        {
            PlanOfMarker.Add(new Marker(position), new MarkerStruct());
        }


        /// <summary>
        /// Добавление подзадачи в список задач с условием ИЛИ
        /// </summary>
        /// <param name="newMarker">Добавляемая задача</param>
        /// <param name="markerInPlan">Задача, к которой нужно добавить подзадачу</param>
        public void AddMarkerIntoMarkerStruct(Marker newMarker, Marker markerInPlan)
        {
            if (PlanOfMarker.ContainsKey(markerInPlan)) // если в словаре есть такая задача
            {   // работаем со списком списков её (задачи) связей (в данном случае - со внешним)
                MarkerStruct markerStructure = PlanOfMarker[markerInPlan]; // взяли из словаря значение (список списков)
                markerStructure.AddDisjunction(newMarker); // добавили таск в дизьюнкцию (во внешний список)
                PlanOfMarker[markerInPlan] = markerStructure; // обновили словарь
            }
        }


        /// <summary>
        /// Добавление подзадачи в список задач с условием И
        /// </summary>
        /// <param name="newMarker">Добавляемая задача</param>
        /// <param name="markerInPlan">Задача в плане, к которой происходит добавление</param>
        /// <param name="markerInStruct">Задача в структуре, по которой ищется нужный список</param>
        public void AddMarkerIntoMarkerStruct(Marker newMarker, Marker markerInPlan, Marker markerInStruct)
        {
            if (PlanOfMarker.ContainsKey(markerInPlan))
            {
                MarkerStruct markerStructure = PlanOfMarker[markerInPlan];
                markerStructure.AddConjunction(newMarker, markerInStruct);
            }
        }
        #endregion

        #region DeleteMarker
        /// <summary>
        /// Удаление маркера из плана
        /// </summary>
        /// <param name="deletedMarker">Удаляемый маркер</param>
        public void DeleteMarker(Marker deletedMarker)
        {
            // перебор словаря
            foreach (KeyValuePair<Marker, MarkerStruct> markerStruct in PlanOfMarker)
            {
                // зачистка связей всех маркеров с удаляемым маркером
                for (int i=0;i<markerStruct.Value.Disjunction.Count;i++)
                {
                    markerStruct.Value.RemoveElement(deletedMarker);
                }
            }
            PlanOfMarker.Remove(deletedMarker); // убираем маркер из словаря (плана)
        }
        #endregion

        #region Создание задачи по маркеру
        /// <summary>
        /// Собираем задачу по маркеру
        /// </summary>
        public void MarkerToGoal()
        {
            // перебор всех маркеров
            foreach (Marker m in PlanOfMarker.Keys)
            {
                // из каждого маркера создаётся задача и добавляется в итоговый план
                Plan.Add(new FPGoal(m.Goal, m.Action, m.DemandModel, m.СonditionsСontext,
                    m.Time, m.Position), new GoalStruct());
            }
        }

        /// <summary>
        /// Перенос связей между маркерами в связи между задачами
        /// </summary>
        public void MarkerStructToGoalStruct()
        {
            List<Tuple<Marker, FPGoal>> first = new List<Tuple<Marker, FPGoal>>();
            // для создания списка тьюплов "маркер-задача" переходим от словаря к массиву ключей
            ICollection<Marker> markers = PlanOfMarker.Keys;
            var markersArray = markers.ToArray();
            ICollection<FPGoal> goals = Plan.Keys;
            var goalsArray = goals.ToArray();

            // создаём список тьюплов
            for (int i = 0; i < PlanOfMarker.Count; i++)
            {
                first.Add(new Tuple<Marker, FPGoal>(markersArray[i], goalsArray[i]));
            }

            List<FPGoal> current; // список задач для конъюнкции
            foreach (Marker m in markers)
            {
                List<List<Marker>> outList = PlanOfMarker[m].Disjunction; // граф связи маркеров
                GoalStruct gs = new GoalStruct(); // пустой граф связи задач

                // перебор дизъюнкции
                foreach (var inList in outList)
                {
                    current = new List<FPGoal>();
                    // TODO с каждой итерацией добавляем элемент в дизъюнкцию
                    foreach (var mark in inList)
                    {
                        var find = (first.Find(t => t.Item1 == mark).Item2); // находим задачу соответствующую маркеру
                        current.Add(find); // добавление задачи в конъюнкцию
                    }
                    gs.AddDisjunction(current); // пополнение графа задач (добавление в дизъюнкцию)
                }
                Plan[first.Find(t => t.Item1 == m).Item2] = gs; // обновление связей для соответствующей задачи в плане
            }
        } 
        #endregion

        #region CreatePath

        /// <summary>
        /// Прокладка пути 
        /// </summary>
        /// <param name="map">Карта</param>
        /// <param name="start">Начальная точка</param>
        /// <param name="finish">Конечная точка</param>
        /// <returns>Список точек, по которым нужно двигаться</returns>
        public List<CellPosition> CreatePath(FPMap.FPMap map, CellPosition start, CellPosition finish)
        {
            AlgorithmLi algorithm = new AlgorithmLi(start, finish, map.Massiv, true);
            List<CellPosition> path = algorithm.Path();
            return path;
        }

        #endregion
    }
}
