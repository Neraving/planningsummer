﻿using System;
using System.Collections.Generic;
using System.Linq;
using AbstractCommunication;
using AbstractImplementation;
using AbstractModels;
using AbstractShared;
using FPGeometry;
using FPMap;
using FPModels.Plan;
using FPModels.States;
using Planner;


namespace Engine.Planner
{
    ///// <summary>
    ///// Тестирование системы
    ///// </summary>
    //[Serializable]
    //public class TestSearch : ControlComponent<FPGroupState>, IPlanner<FPGroupState>
    //{
    //    public TestSearch(POCId me, ICommSwitch commSwitch, params ICommunication[] comms)
    //        : base(me, commSwitch, comms)
    //    {
    //    }

    //    #region смешные методы

    //    public FPPlan GetPlan()
    //    {
    //        double flyingHihgt = 50;
    //        var fragmentsList = CreateFragments(flyingHihgt);
    //        var map = new FPMap.FPMap();
    //        map.CreateMapFragments();

    //        // --------------------------------
    //        Editor e = new Editor();
    //        Marker marker = e.AddMarker(new Square(new Point2D(0.2, 0.8), 0.6));
    //        FPGoal mainTask = e.CreateGoal(marker);
    //        FPPlan plan = e.CreatePlan(mainTask);

    //        FPGoal coord = e.CreateGoal(marker, "Получить координаты банана");
    //        e.AddGoal(plan, coord, mainTask);

    //        FPGoal getMap = e.CreateGoal(marker, "Получить карту");
    //        e.AddGoal(plan, getMap, coord);

    //        FPGoal loadMap = e.CreateGoal(marker, "Загрузить карту из хранилища");
    //        e.AddGoal(plan, loadMap, getMap);

    //        FPGoal createMap = e.CreateGoal(marker, "Создать карту");
    //        e.AddGoal(plan, createMap, getMap);

    //        FPGoal getImage = e.CreateGoal(marker, "Получить изображение поверхности");
    //        e.AddGoal(plan, getImage, createMap);

    //        // --------------------------------

    //        /*
    //        var goal = new FPGoal("Найти банан");
    //        goal.Position = null; // TODO Позиция нашей базы
    //        FPPlan plan = new FPPlan(goal);

    //        TaskStruct structBananaPosition = new TaskStruct();
    //        var taskBananaPosition = new FPGoal("Получить координаты банана");
    //        structBananaPosition.AddDisjunction(taskBananaPosition);

    //        TaskStruct structSearch = new TaskStruct();
    //        var taskFindMap = new FPGoal("Получить карту");
    //        structSearch.AddDisjunction(taskFindMap);
    //        plan.Plan.Add(taskBananaPosition, structSearch);

    //        TaskStruct structFindMap = new TaskStruct();
    //        var taskLoadMap = new FPGoal("Загрузить карту из хранилища"); // TODO Загружать, если карта не старая
    //        var taskMakeMap = new FPGoal("Создать карту");
    //        structFindMap.AddDisjunction(taskLoadMap);
    //        structFindMap.AddDisjunction(taskMakeMap);
    //        plan.Plan.Add(taskFindMap, structFindMap);

    //        TaskStruct structMakeMap = new TaskStruct();
    //        var taskMakeImage = new FPGoal("Получить изображение поверхности");
    //        taskMakeImage.Position = new Square(new Point2D(0, 0), 0);
    //        taskMakeImage.Center = null; // TODO Поставить свойство Центр в квадрат. Подумать о добавлении центра фигуры.
    //        structMakeMap.AddDisjunction(taskMakeImage);
    //        plan.Plan.Add(taskMakeMap, structMakeMap);*/
    //        return plan;
    //    }

    //    public FPMap.FPMap GetMap()
    //    {
    //        double flyingHihgt = 50;
    //        var fragmentsList = CreateFragments(flyingHihgt); // TODO проверить, почему не используется
    //        var map = new FPMap.FPMap();
    //        map.CreateMapFragments();
    //        return map;
    //    }

    //    public FPMap.FPMap GetOldMap()
    //    {
    //        double flyingHihgt = 50;
    //        var fragmentsList = CreateFragments(flyingHihgt);
    //        var map = new FPMap.FPMap(1, fragmentsList, 10, flyingHihgt);
    //        map.CreateMapFragments();
    //        return map;
    //    }

    //    public List<FPFragment> CreateFragments(double flyingHight)
    //    {
    //        var fragmentsList = new List<FPFragment>();

    //        // дерево, высота 17
    //        Circle treePosition = new Circle(new Point2D(4.5, 5.5), 0.4);
    //        FPFragment tree = new FPFragment(treePosition, height: 17);

    //        // люди, высота 3
    //        var peoplePoints = new List<Point2D>();
    //        peoplePoints.Add(new Point2D(4.9, 1.1));
    //        peoplePoints.Add(new Point2D(4.9, 1.9));
    //        peoplePoints.Add(new Point2D(1.1, 1.9));
    //        peoplePoints.Add(new Point2D(1.1, 1.1));
    //        ConvexPolygon peoplePosition = new ConvexPolygon(peoplePoints);
    //        FPFragment people = new FPFragment(peoplePosition, height: 3);

    //        // пень, высота 1
    //        var stumpPoints = new List<Point2D>();
    //        stumpPoints.Add(new Point2D(2.5, 7.1));
    //        stumpPoints.Add(new Point2D(3.3, 7.2));
    //        stumpPoints.Add(new Point2D(3.7, 7.4));
    //        stumpPoints.Add(new Point2D(3.8, 8.2));
    //        stumpPoints.Add(new Point2D(3.15, 8.6));
    //        stumpPoints.Add(new Point2D(2.47, 8.88));
    //        stumpPoints.Add(new Point2D(1.4, 8.07));
    //        stumpPoints.Add(new Point2D(1.64, 7.6));
    //        stumpPoints.Add(new Point2D(1.8, 7.12));
    //        ConvexPolygon stumpPosition = new ConvexPolygon(stumpPoints);
    //        FPFragment stump = new FPFragment(stumpPosition, height: 1);

    //        // кусты, высота 2
    //        var shrubsPoints = new List<Point2D>();
    //        shrubsPoints.Add(new Point2D(6.9, 8.1));
    //        shrubsPoints.Add(new Point2D(6.9, 9.1));
    //        shrubsPoints.Add(new Point2D(9.9, 9.9));
    //        shrubsPoints.Add(new Point2D(6.1, 9.9));
    //        shrubsPoints.Add(new Point2D(6.1, 8.1));
    //        ConvexPolygon shrubsPosition = new ConvexPolygon(shrubsPoints);
    //        FPFragment shrubs = new FPFragment(shrubsPosition, height: 2);

    //        // дом, высота 30
    //        var housePoints = new List<Point2D>();
    //        housePoints.Add(new Point2D(9.8, 5.2));
    //        housePoints.Add(new Point2D(9.8, 8.8));
    //        housePoints.Add(new Point2D(7.2, 8.8));
    //        housePoints.Add(new Point2D(7.2, 6.5));
    //        housePoints.Add(new Point2D(6.2, 6.5));
    //        housePoints.Add(new Point2D(6.2, 5.2));
    //        ConvexPolygon housePosition = new ConvexPolygon(housePoints);
    //        AddPolygons(fragmentsList, housePosition, 30);

    //        // наземный робот №1. 
    //        Square rover1Position = new Square(new Point2D(9.2, 2.8), 0.6);
    //        FPFragment roverFirst = new FPFragment(rover1Position, height: 1);

    //        // наземный робот №2
    //        Square rover2Position = new Square(new Point2D(4.2, 3.8), 0.6);
    //        FPFragment roverSecond = new FPFragment(rover2Position, height: 1);

    //        // наземный робот №3
    //        Square rover3Position = new Square(new Point2D(4.2, 8.8), 0.6);
    //        FPFragment roverThird = new FPFragment(rover3Position, height: 1);

    //        // квадрокоптер №1 
    //        Circle quard1Position = new Circle(new Point2D(1.5, 4.5), 0.4);
    //        Point2D quard1Centre = (Point2D)quard1Position.Center;
    //        FPFragment quardFirst = new FPFragment(quard1Position, height: flyingHight + 1);

    //        // квадрокоптер №2
    //        Circle quard2Position = new Circle(new Point2D(7.5, 1.5), 0.4);
    //        Point2D quard2Centre = (Point2D)quard2Position.Center;
    //        FPFragment quardSecond = new FPFragment(quard2Position, height: flyingHight + 1);

    //        // квадрокоптер №3
    //        Circle quard3Position = new Circle(new Point2D(5.5, 9.5), 0.4);
    //        Point2D quard3Centre = (Point2D)quard3Position.Center;
    //        FPFragment quardThird = new FPFragment(quard3Position, height: flyingHight + 1);

    //        fragmentsList.Add(tree);
    //        fragmentsList.Add(people);
    //        fragmentsList.Add(stump);
    //        fragmentsList.Add(shrubs);
    //        fragmentsList.Add(roverFirst);
    //        fragmentsList.Add(roverSecond);
    //        fragmentsList.Add(roverThird);
    //        fragmentsList.Add(quardFirst);
    //        fragmentsList.Add(quardSecond);
    //        fragmentsList.Add(quardThird);

    //        return fragmentsList;
    //    }

    //    /// <summary>
    //    /// Разбивает невыпуклый многоугольник на выпуклые.
    //    /// Добавляет получившиеся части в список фрагментов на карте.
    //    /// </summary>
    //    /// <param name="fragmentsList">Список фрагментов</param>
    //    /// <param name="polygon">Невыпуклый многоугольник</param>
    //    /// <param name="hight">Высота</param>
    //    public void AddPolygons(List<FPFragment> fragmentsList, Polygon polygon, double hight)
    //    {
    //        var list = polygon.ConvertToConvexList(polygon.Points.ToList());// TODO после переделки  геометрии убрать конверсию  в список
    //        foreach (List<Point2D> fr in list)
    //        {
    //            ConvexPolygon partPolygon = new ConvexPolygon(fr);
    //            FPFragment fragment = new FPFragment(partPolygon, height: hight);
    //            fragmentsList.Add(fragment);
    //        }
    //    }

    //    #endregion





    //    protected override void ProcessMapMessage(IMapMessage mapMsg)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    protected override void ProcessStatusMessage(IStateMessage statusMsg)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    protected override void ProcessRawDataMessage(IRawDataMessage rawDataMsg)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    protected override void ProcessModelMessage(IModelMessage modelMsg)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    protected override void ProcessFormalGoalMessage(IFormalGoalMessage taskMsg)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    protected override void ProcessOrderMessage(IOrderMessage orderMsg)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    protected override void ProcessRequestMessage(IRequestMessage requestMsg)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override bool ValidateCommunicatorId(POCId id)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public POCId Me { get; protected set; }

    //    public ICommSwitch Link { get; protected set; }




    //    #region Implementation of IPlanner<FPGroupState>

    //    public void LoadMap(IMap map)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public void StartPlanning(IGoal goal)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public event EventHandler PlanningOver;

    //    #endregion




    //}

}
