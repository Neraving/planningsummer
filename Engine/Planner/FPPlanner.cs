﻿using System;
using System.Linq;
using AbstractCommunication;
using AbstractImplementation;
using AbstractModels;
using AbstractShared;
using FPShared;
using FPShared.Identificators;
using CommunicationClient;
using Planner;

namespace Engine.Planner
{
    public class FPPlanner : ControlComponent<IOperatedState>, IPlanner<IOperatedState>
    {
        /// <summary>
        /// Проверяет разрешение других участников связи на передачу сообщений нам.
        /// </summary>
        /// <remarks>Используется для выявления "незаконных" участников связи.
        /// При реализации этой функции следует как правило руководствоваться данными, наличными у источника связи - например, таблицей позывных.
        /// </remarks>
        /// <param name="id">Автор присланного сообщения.</param>
        /// <returns><code>true</code> если мы должны получать сообщения от этого автора.</returns>
        public override bool ValidateCommunicatorId(POCId id)
        {
            return true; //TODO реализовать
        }

        /// <summary>
        /// Метод замены рабочей карты планироващика.
        /// </summary>
        /// <param name="map">Карта для замены.</param>
        public void LoadMap(IMap map)
        {
            //TODO реализовать.
        }

        /// <summary>
        /// Событие возникает тогда, когда планирование завершено (успешно или неуспешно).
        /// </summary>
        public event EventHandler PlanningOver;

        /// <summary>
        /// Метод начинает планирование.
        /// Скорее асинхронный.
        /// </summary>
        /// <param name="goal">Формальная задача для планирования.</param>
        public void StartPlanning(IGoal goal)
        {
            //TODO реализовать.
        }


        protected override void ProcessMapMessage(IMapMessage mapMsg)
        {
            Map = mapMsg.Map;
        }

        protected override void ProcessStatusMessage(IStateMessage statusMsg)
        {
            throw new NotImplementedException();
        }

        protected override void ProcessRawDataMessage(IRawDataMessage rawDataMsg)
        {
            throw new NotImplementedException();
        }

        protected override void ProcessModelMessage(IModelMessage modelMsg)
        {
            throw new NotImplementedException();
        }

        protected override void ProcessFormalGoalMessage(IFormalGoalMessage taskMsg)
        {
            throw new NotImplementedException();
        }

        protected override void ProcessOrderMessage(IOrderMessage orderMsg)
        {
            throw new NotImplementedException();
        }

        protected override void ProcessRequestMessage(IRequestMessage requestMsg)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="timeInterval"></param>
        /// <param name="communications"></param>
        public FPPlanner(POCId id, int timeInterval, ICommSwitch commSwitch, params ICommunication[] communications)
            : base(id, commSwitch, communications)
        {
            //this.Id = id;
            //TestSearch test = new TestSearch();
            //this.PlanGenerator = test.Main();
        }


        /// <summary>
        /// Обработка входящего сообщения
        /// </summary>
        /// <param name="message">входящее сообщение</param>
        public override void ProcessIncoming(IMessage message)
        {
            var recipientId = message.To;
            if (recipientId.Is(Me))
            {
                //принимаем от Система хранения данных
                //по нашему запросу RequestMessage
                if (message is IMapMessage)
                {
                    if (this.ChiefId== message.From) //todo Повеситься
                    else if (NeighboursId.ToList().Contains(message.From)) //todo еще ченить
                    else if (SubordinatesId.ToList().Contains(message.From)) 
                    else //TODO DO NOTHING
                    ProcessMapMessage((IMapMessage)message);
                }
                //TODO Подумать, нужен ли????
                else if (message is IStateMessage)
                {
                    ProcessStatusMessage((IStateMessage)message);
                }
                else if (message is IRawDataMessage)
                {
                    ProcessRawDataMessage((IRawDataMessage)message);
                }
                //принимаем от  Система хранения данных
                //только по нашему запросу RequestMessage
                    // 
                else if (message is IModelMessage)
                {
                    ProcessModelMessage((IModelMessage)message);
                }
                else if (message is IFormalGoalMessage)
                {
                    ProcessFormalGoalMessage((IFormalGoalMessage)message);
                }
                else if (message is IOrderMessage)
                {
                    ProcessOrderMessage((IOrderMessage)message);
                }
                //принимаем RequestMessage от Система оперативного управления
                //наш ответ OrderMessage
                    // только от соседа ?
                else if (message is IRequestMessage)
                {
                    ProcessRequestMessage((IRequestMessage)message);
                }
            }
        }
    }
}
