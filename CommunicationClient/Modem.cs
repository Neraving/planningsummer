﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using AbstractCommunication;
using AbstractShared;

namespace CommunicationClient
{
    public class Modem : ICommunication
    {
        private Socket m_sock;
        private byte[] m_byBuff = new byte[100000];
        private List<byte> buf = new List<byte>();


        public Modem(ComId id)
        {
            var m = new FPMap.FPMap();
            Id = id;
            var serverAddress = "192.168.0.75";

            if (m_sock != null && m_sock.Connected)
            {
                m_sock.Shutdown(SocketShutdown.Both);
                System.Threading.Thread.Sleep(10);
                m_sock.Close();
            }

            m_sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            IPEndPoint epServer = new IPEndPoint(IPAddress.Parse(serverAddress), 399);

            m_sock.Blocking = false;
            AsyncCallback onconnect = new AsyncCallback(OnConnect);
            m_sock.BeginConnect(epServer, onconnect, m_sock);
        }

        public void OnConnect(IAsyncResult ar)
        {
            Socket sock = (Socket)ar.AsyncState;
            try
            {
                if (sock.Connected)
                    SetupRecieveCallback(sock);
                else
                    throw new ArgumentException("Connect Failed!");
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void OnRecievedData(IAsyncResult ar)
        {
            Socket sock = (Socket)ar.AsyncState;
            try
            {
                int nBytesRec = sock.EndReceive(ar);
                if (nBytesRec > 0)
                {
                    var b = m_byBuff.Take(nBytesRec).ToArray();
                    buf.AddRange(b);
                    var obj = ByteArrayToObject(b);
                    RecieveFromMedia(obj as IMessage);
                    SetupRecieveCallback(sock);
                    m_byBuff = new byte[100000];
                }
                else
                {
                    sock.Shutdown(SocketShutdown.Both);
                    sock.Close();
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void SetupRecieveCallback(Socket sock)
        {
            try
            {
                AsyncCallback recieveData = new AsyncCallback(OnRecievedData);
                sock.BeginReceive(m_byBuff, 0, m_byBuff.Length, SocketFlags.None, recieveData, sock);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void Send(IMessage message)
        {
            if (m_sock == null || !m_sock.Connected)
            {
                return;
            }
            try
            {
                Byte[] byteDateLine = ObjectToByteArray(message);
                m_sock.BeginSend(byteDateLine, 0, byteDateLine.Length, 0, new AsyncCallback(SendCallback), m_sock);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                ms.Flush();
                return ms.ToArray();
            }
        }

        private Object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);
            return obj;
        }

        /// <summary>
        /// Служебный метод для использования средой при доставке сообщения. Приводит к возникновению события IncomingMessage.
        /// </summary>
        /// <param name="message">Сообщение, принятое из среды связи.</param>
        public void RecieveFromMedia(IMessage message)
        {
            if (IncomingMessage != null)
            {
                IncomingMessage(this, new IncomingMessageEventArgs(message));
            }
        }

        public event EventHandler CommunicationError;

        public ComId Id { get; protected set; }

        public event IncomingMessageEvent IncomingMessage;

        public IEnumerable<POCId> AvailableParticipants()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMessage> Receive(POCId receiver, Predicate<IMessage> filter = null)
        {
            throw new NotImplementedException();
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;
                Byte[] endOfStream = ObjectToByteArray("EOF");
                client.BeginSend(endOfStream, 0, endOfStream.Length, 0, new AsyncCallback(SendCallback), endOfStream);
                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);

                // Signal that all bytes have been sent.
                //sendDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
