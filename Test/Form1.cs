﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AbstractCommunication;
using AbstractModels;
using AbstractShared;
using CommunicationClient;
using Engine.Planner;
using FPShared;
using FPShared.Identificators;
using Variables;

namespace Test
{
    public partial class Form1 : Form
    {
        static Modem rad1 = new Modem(new FPComId("3"));
        static FPPOCId fpid1 = new FPPOCId("3");
        static FPPOCId fpid2 = new FPPOCId("4");
        static CommSwitch commSwitch = new CommSwitch();
        static FPPlanner mon1 = new FPPlanner(fpid1, 2, commSwitch, new ICommunication[] { rad1 });

        public Form1()
        {
            InitializeComponent();
            commSwitch.AddCommunication(fpid1, rad1);
            commSwitch.AddCommunication(fpid2, rad1);
        }
        
        private void button1_Click(object sender, EventArgs e)
        {

            TestSearch bananaSearch = new TestSearch();
            var map = bananaSearch.GetOldMap();
            var recipient = new FPPOCId("123");
            var message = new FPModels.Messages.InsertDataMessage(map, fpid1, recipient);
            mon1.Link[recipient].Send(message); //TODO проверить, нужно ли это вообще
        }



        [Serializable]
        public class DataMessage : IMessage, IRawDataMessage
        {
            public DataMessage(string d, FPPOCId from, FPPOCId to)
            {
                Data = d;
                From = from;
                To = to;
            }

            public DateTime CreationTime
            {
                get;
                protected set;
            }

            public POCId From
            {
                get;
                protected set;
            }

            public DateTime SendingTime
            {
                get { throw new Exception(); }
                set { throw new Exception(); }
            }

            public POCId To
            {
                get;
                protected set;
            }


            public object Data
            {
                get;
                protected set;
            }
        }
    }
}
