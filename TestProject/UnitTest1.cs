﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using AbstractModels;
using Engine;
using Engine.Planner;
using FPGeometry;
using FPModels;
using FPModels.Plan;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Variables;

namespace TestProject
{
    [TestClass]
    public class UnitTest1
    {


        #region Marker.cs tests
        // TODO дописать тест
        [TestMethod]
        public void TestAddContext()
        {
            Predicate<int> pred = (x => x > 5);
            Predicate<string> pred2 = (x => x == "");
            Marker her = new Marker(new Point(2, 3), "123");
            СonditionsСontext cc = new СonditionsСontext(new Tuple<string, object>("", pred));
            her.ChangeContext(cc);
            //her.AddContext(1,new ConstrainedVariable("azaza",pred2.GetType().GetGenericArguments()[0], pred2) );
        }

        [TestMethod]
        public void TestDelContext()
        {
            Predicate<int> pred = (x => x > 5);
            Predicate<string> pred2 = (x => x == "");
            Predicate<string> pred3 = (x => x == "5");
            Predicate<string> pred4 = (x => x == "6");
            var самаякрутаяпеременнаянасвете = new Marker(new Point(2, 3), "123");
            СonditionsСontext cc = new СonditionsСontext(new Tuple<string, object>("", pred));
            cc.AddCondition(new Tuple<string, object>("1234", pred3));
            cc.AddCondition(new Tuple<string, object>("12345", pred2));
            cc.AddCondition(new Tuple<string, object>("12346", pred4));
            самаякрутаяпеременнаянасвете.ChangeContext(cc);
            самаякрутаяпеременнаянасвете.DeleteContext(1);
            самаякрутаяпеременнаянасвете.DeleteContext("12346");
            самаякрутаяпеременнаянасвете.DeleteContext();

        }

        [TestMethod]
        public void TestAddDeamanModel()
        {
            Marker her = new Marker(new Point(2, 3), "banan");
            her.AddDemandModel(AbleToAction.Fly);
            her.AddDemandModel(new List<AbleToAction>() { AbleToAction.Photo, AbleToAction.Move });
        }

        [TestMethod]
        public void TestDeleteDemandModel()
        {
            Marker her = new Marker(new Point(2, 3), "banan");
            her.AddDemandModel(new List<AbleToAction>() { AbleToAction.Photo, AbleToAction.Move, AbleToAction.Fly });
            her.DeleteDemandModel(AbleToAction.Move);
            her.DeleteDemandModel(AbleToAction.Video);
            her.DeleteDemandModel();
        }

        [TestMethod]
        public void TestOperationsWithAction()
        {
            Marker her = new Marker(new Point(2, 3), "banan");
            her.CreateAction();
            her.AddAction(new FPAction());
            her.DeleteAction();
        }
        #endregion

        #region MarkerStruct.cs tests
        [TestMethod]
        public void TestAddConjunctionDisjunction()
        {
            MarkerStruct markerStruct = new MarkerStruct();
            Dictionary<Marker, MarkerStruct> PlanOfMarker = new Dictionary<Marker, MarkerStruct>();

            Marker m1 = new Marker(new Point(1, 1), "1");
            Marker m2 = new Marker(new Point(2, 2), "2");
            Marker m3 = new Marker(new Point(3, 3), "3");
            Marker m4 = new Marker(new Point(4, 4), "4");
            Marker m5 = new Marker(new Point(5, 5), "5");
            Marker m6 = new Marker(new Point(6, 6), "6");

            // 2: 4 & 5
            // 5: (1 || 3 || 6)
            PlanOfMarker.Add(m1, new MarkerStruct());
            PlanOfMarker.Add(m2, new MarkerStruct());
            PlanOfMarker.Add(m3, new MarkerStruct());
            PlanOfMarker.Add(m4, new MarkerStruct());
            PlanOfMarker.Add(m5, new MarkerStruct());


            markerStruct.AddDisjunction(m5); // добавление в дизъюнкцию одного маркера
            markerStruct.AddConjunction(m1, m5); // добавление в конъюнкцию к одному маркеру другого
            // добавление в конънкцию к одному маркеру несколько других
            markerStruct.AddConjunction(new List<Marker>() { m3, m6 }, m5);
            // добавление в дизъюнкцию несколько маркеров
            markerStruct.AddDisjunction(new List<Marker>() { m2, m4 });
        }

        [TestMethod]
        public void TestRemoveElement()
        {
            MarkerStruct markerStruct = new MarkerStruct();
            Dictionary<Marker, MarkerStruct> PlanOfMarker = new Dictionary<Marker, MarkerStruct>();

            Marker m1 = new Marker(new Point(1, 1), "1");
            Marker m2 = new Marker(new Point(2, 2), "2");
            Marker m3 = new Marker(new Point(3, 3), "3");
            Marker m4 = new Marker(new Point(4, 4), "4");
            Marker m5 = new Marker(new Point(5, 5), "5");
            Marker m6 = new Marker(new Point(6, 6), "6");

            // главная: 2 || 5 || 6
            // 2: 3 & 4
            // 5: 1
            PlanOfMarker.Add(m1, new MarkerStruct());
            PlanOfMarker.Add(m2, new MarkerStruct());
            PlanOfMarker.Add(m3, new MarkerStruct());
            PlanOfMarker.Add(m4, new MarkerStruct());
            PlanOfMarker.Add(m5, new MarkerStruct());

            markerStruct.AddDisjunction(new List<Marker>() { m2, m3, m4 });
            markerStruct.AddDisjunction(new List<Marker>() { m5, m1});
            markerStruct.AddDisjunction(m6);
            

            markerStruct.RemoveElement(m6);
            markerStruct.RemoveElement(m3);

        } 
        #endregion




        [TestMethod]
        public void TestMarkerToGoal()
        {
            // + захватывает работоспособность public List<Marker> FindMarker(Marker marker)
            Dictionary<Marker, MarkerStruct> PlanOfMarker = new Dictionary<Marker, MarkerStruct>();
            Marker m1 = new Marker(new Point(1, 1), "1");
            Marker m2 = new Marker(new Point(2, 2), "2");
            Marker m3 = new Marker(new Point(3, 3), "3");
            Marker m4 = new Marker(new Point(4, 4), "4");
            Marker m5 = new Marker(new Point(5, 5), "5");

            MarkerStruct ms2 = new MarkerStruct();
            ms2.AddDisjunction(new List<Marker>() { m4 });
            MarkerStruct ms4 = new MarkerStruct();
            ms4.AddDisjunction(new List<Marker>() { m5 });
            MarkerStruct ms5 = new MarkerStruct();
            ms5.AddDisjunction(new List<Marker>() { m1 });
            ms5.AddDisjunction(new List<Marker>() { m3, m2 });

            PlanOfMarker.Add(m1, new MarkerStruct());
            PlanOfMarker.Add(m2, ms2);
            PlanOfMarker.Add(m3, new MarkerStruct());
            PlanOfMarker.Add(m4, ms4);
            PlanOfMarker.Add(m5, ms5);

            Engine.Planner.Editor editor = new Editor(PlanOfMarker);
            editor.MarkerToGoal();

            // public void RemoveElement(Marker marker)
            ms5.RemoveElement(m2);

            editor.MarkerStructToGoalStruct();
        }

        


        [TestMethod]
        public void TestMethod1()
        {

            //double x = 2.5;
            //double y = 7.0;
            //Assert.AreEqual(x, y, 0.001, "Ebat' copat'");

            double beginningBalance = 11.99;
            double debitAmount = 4.55;
            double expected = 7.44;

            // act
            //account.Debit(debitAmount);
            bool a = expected < beginningBalance;
            // assert
            double actual = beginningBalance;
            Assert.IsTrue(expected < beginningBalance);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
        [TestMethod]
        public void TestMethod2()
        {
        //    FPGoal goal = new FPGoal("Banan"); 
        //    Engine.Planner.Editor editor = new Editor();
        //    editor.DeleteAction(goal);
        //    Assert.IsTrue(goal.Action==null,"as");
        //    //Assert.Inconclusive("Verify the correctness of this test method.");
        }
        [TestMethod]
        public void TestAddDictionary()
        {
        //    FPGoal goal = new FPGoal("Banan");
        //    Engine.Planner.Editor editor = new Editor();
        //    FPPlan plan = new FPPlan(new FPGoal("as"));

        //    List<Marker> markers = new List<Marker>(){new Marker(new ConvexPolygon(new List<Point>()))};
        //    editor.UpdatePlan(new FPPlan(), markers);
        //    Assert.IsTrue(plan.Plan.Count>0, "as");
        //    //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }

}
